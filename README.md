<h1 style="text-align:center">
Regular Test Pattern Datasets
</h1>
<div style="text-align:center">
<h3>
<a href="https://liris.cnrs.fr/page-membre/iuliia-tkachenko">Iuliia Tkachenko, </a>
<a href="https://perso.univ-st-etienne.fr/tremeaua/">Alain Trémeau</a>
<br>
<br>
</h3>
</div>

# Table of content
- [Overview](#overview)
- [Dataset](#dataset)
- [Citation](#citation)
- [Acknowledgements](#acknowledgements)

# Overview
This web-page is dedicated to public databases of Regular Test Patterns printed using rotogravure prinitng process using two cylinders C1 and C2 and two presses P1 and P2. These datasets are used in the paper [Authentication of rotogravure print-outs using a regular test pattern](https://hal.archives-ouvertes.fr/hal-03788148/) and [Printed packaging authentication: similarity metric learning for rotogravure manufacture process identification](https://hal.archives-ouvertes.fr/).  Please cite the corresponding papers while using these datasets in your academic work.

# Dataset
We worked with a regular test pattern $E$ (see Fig.a) printed using two presses (P1 and P2) and two types of engraving cylinders: 1) two cylinders engraved using chemical etching (C1 and C2); and 2) a cylinder engraved using electro-mechanical process (M1). 
The samples printed using the cylinders engraved using chemical etching (Fig.b-c) differ a lot from the samples printed using the electro-mechanically engraved cylinder (Fig.d-e).
Additionally, we studied the impact of the printing support used, using the regular test patterns printed on: 1) thin aluminium foils - called blisters (letter 'b' in the table); and 2) thick aluminium foils - called strips (letter 's' in the table). The details about the data set used are given in the following Table:

Press&Cylinder |  E1   |  E2   |  E3   | E4    |  E5   |  E6      
:---: | :---: | :----:| :----:| :----:| :----:| :----:
P1C1s |  50   |  50   |   50  |  50   |  50   |  50 
P2C1s |  50   |  50   |   50  |  50   |  50   |  50
P1C2s |  50   |  50   |   50  |  50   |  50   |  50 
P2C2s |  50   |  50   |   50  |  50   |  50   |  50 
P1C1b |  50   |  50   |   50  |  50   |  50   |  50 
P2C1b |  50   |  50   |   50  |  50   |  50   |  50 
P1M1s |  50   |  50   |   50  |  50   |  50   |  50 
P2M1s |  49   |  48   |   49  |  48   |  49   |  48 

##### **Example of genuine and fake document from PaySlip dataset**
![Augmented PaySlip samples](assets/reg_pattern_samples.png)

# Citation
The dataset could only be used for scientific purposes. It must not be republished other than by the original authors. The scientific use includes processing the data and showing it in publications and presentations. If you use it, please cite the following papers.

Journal paper about cylinder signature identifiaction:
```
@article{tkachenko2022authentication,
  title={Authentication of rotogravure print-outs using a regular test pattern},
  author={Tkachenko, Iuliia and Tr{\'e}meau, Alain and Fournel, Thierry},
  journal={Journal of Information Security and Applications},
  volume={66},
  pages={103133},
  year={2022},
  publisher={Elsevier}
}
```

Conference paper on VISAPP 2023 about similarity metric learning for press and support identification:
```
@InProceedings{yemelianenko2023printed,
    author    = {Yemelianenko, T., Trémeau, A. and Tkachenko I.},
    title     = {Printed packaging authentication: similarity metric learning for rotogravure manufacture process identification},
    booktitle = {VISIGRAPP (4: VISAPP)},
    month     = {February},
    year      = {2023}
}
```
 
# Acknowledgements
All the printed samples were provided by Sergusa Solutions Pvt Ltd in the context of PackMark project (IFCPAR-7127) supported by the Indo-French Center for the Promotion of Advanced Research.

